<?php

namespace Chesio\LazyDaisy;

class Plugin
{
    /**
     * @var int Output buffer is initialized from within `template_redirect` hook with given priority.
     */
    const PRIORITY = 5;

    /**
     * @var string Regular expression to match all HTML images.
     */
    const IMG_REGEXP = '#(<img )([^>]+?)(/?>)#si';


    /**
     * @var string Plugin filename
     */
    private $plugin_filename;


    /**
     * Construct the plugin instance.
     *
     * @param string $plugin_filename Plugin filename
     */
    public function __construct(string $plugin_filename)
    {
        $this->plugin_filename = $plugin_filename;
    }


    /**
     * Load the plugin by hooking into WordPress actions and filters.
     */
    public function load()
    {
        // Register initialization method.
        add_action('init', [$this, 'init'], 10, 0);
    }


    /**
     * Perform initialization tasks.
     *
     * @action https://developer.wordpress.org/reference/hooks/init/
     */
    public function init()
    {
        add_action('template_redirect', [$this, 'startOutputBuffering'], self::PRIORITY, 0);
    }


    public function enqueueAssets()
    {
        wp_register_script('intersection-observer', plugins_url('assets/intersection-observer.js', $this->plugin_filename), [], '0.7.0', true);

        // Always load minified version of Yall script, see:
        // https://github.com/malchata/yall.js/issues/48
        $src = plugins_url('assets/yall.min.js', $this->plugin_filename);
        // (Optionally) declare intersection observer polyfill as dependency.
        $dependencies = apply_filters(Hooks::INSERT_INTERSECTION_OBSERVER_POLYFILL, true) ? ['intersection-observer'] : [];

        wp_enqueue_script('yall.js', $src, $dependencies, '3.1.7', true);
        // Invoke Yall.js on DOMContentLoaded automatically, unless another script overrides our loader.
        wp_add_inline_script('yall.js', "(function(document, window, yall) { window.lazyDaisy = window.lazyDaisy || yall; document.addEventListener('DOMContentLoaded', window.lazyDaisy); })(document, window, yall);");
    }


    /**
     * Start output buffering if not instructed to skip it.
     */
    public function startOutputBuffering()
    {
        if (!$this->skipLazyLoad()) {
            add_action('wp_enqueue_scripts', [$this, 'enqueueAssets'], 10, 0);

            ob_start([$this, 'handleOutputBuffer']);
        }
    }


    /**
     * @param string $buffer
     * @return string
     */
    public function handleOutputBuffer(string $buffer): string
    {
        return preg_replace_callback(self::IMG_REGEXP, [$this, 'injectLazyLoad'], $buffer);
    }


    /**
     * Inject lazy load markup for yall.js:
     * 1. class lazy is inserted
     * 2. src attribute is renamed to data-src
     * 3. srcset attribute is renamed to data-srcset
     * 4. an empty inline image is assigned to src attribute
     *
     * @param array $matches
     * @return string
     */
    public function injectLazyLoad(array $matches): string
    {
        $img_atts = wp_kses_hair($matches[2], ['data', 'http', 'https']);

        if ($this->skipImage($img_atts)) {
            // Image must not be lazy loaded, return the <img /> tag as it is.
            return $matches[0];
        }

        // Process <img /> attributes:
        $atts = [];
        foreach ($img_atts as $key => $props) {
            switch ($key) {
                case 'class':
                    // Class "lazy" must be set.
                    $atts['class'] = 'lazy' . ' ' . $props['value'];
                    break;
                case 'src':
                case 'srcset':
                    $atts["data-$key"] = $props['value'];
                    break;
                default:
                    $atts[$key] = $props['value'];
            }
        }

        if (!isset($atts['class'])) {
            // Class "lazy" must be set.
            $atts['class'] = 'lazy';
        }

        $atts['src']
            // If image width and height (ie. image aspect ratio) are known ...
            = (isset($atts['width']) && isset($atts['height']))
            // ... use embedded SVG as image placeholder to maintain image aspect ratio.
            // See: https://css-tricks.com/preventing-content-reflow-from-lazy-loaded-images/
            ? ("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='" . sprintf("0 0 %s %s", $atts['width'], $atts['height']) . "'%3E%3C/svg%3E")
            // ... otherwise use tiniest GIF ever as image placeholder.
            // See: http://probablyprogramming.com/2009/03/15/the-tiniest-gif-ever
            : 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='
        ;

        // Glue attributes together
        $attributes = implode(
            ' ',
            array_map(
                function (string $key, string $value): string {
                    return sprintf('%s="%s"', $key, $value);
                },
                array_keys($atts),
                array_values($atts)
            )
        );

        return
            $matches[1] . $attributes . $matches[3] // modified <img + attributes + />
            . '<noscript>' . $matches[0] . '</noscript>' // original <img /> for those with JavaScript off
        ;
    }


    /**
     * @param array $img_atts Array of attribute information after parsing by `wp_kses_hair`.
     * @return bool True if image must not be lazy loaded, false otherwise.
     */
    protected function skipImage(array $img_atts): bool
    {
        if (!isset($img_atts['src']) && !isset($img_atts['srcset'])) {
            // No src or srcset to work on.
            return true;
        }

        if (isset($img_atts['data-src']) || isset($img_atts['data-srcset'])) {
            // There is data-src or data-srcset attribute already.
            return true;
        }

        if (isset($img_atts['src']) && (strpos($img_atts['src']['value'], 'data:') === 0)) {
            // Do not lazy load embedded images.
            return true;
        }

        // See: https://core.trac.wordpress.org/ticket/44427#comment:87
        if (isset($img_atts['data-skip-lazy']) || isset($img_atts['class']) && in_array('skip-lazy', explode(' ', $img_atts['class']['value']), true)) {
            // Do not lazy load images marked to skip.
            return true;
        }

        return apply_filters(Hooks::IGNORE_IMAGE, false, $img_atts);
    }


    /**
     * @return bool True if lazy loading should be skipped for current request, false otherwise.
     */
    protected function skipLazyLoad(): bool
    {
        if (is_feed() || is_preview()) {
            return true;
        }

        return apply_filters(Hooks::SKIP_LAZY_LOAD, false);
    }
}
