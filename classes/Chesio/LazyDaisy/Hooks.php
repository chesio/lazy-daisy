<?php

namespace Chesio\LazyDaisy;

interface Hooks
{
    /**
     * @var string
     */
    const INSERT_INTERSECTION_OBSERVER_POLYFILL = 'lazy-daisy/filter:insert-intersection-observer-polyfill';

    /**
     * @var string
     */
    const IGNORE_IMAGE = 'lazy-daisy/filter:ignore-image';

    /**
     * @var string
     */
    const SKIP_LAZY_LOAD = 'lazy-daisy/filter:skip-lazy-load';
}
