# Lazy Daisy

Lazy loads images on your WordPress website with [yall.js](https://github.com/malchata/yall.js).

## Requirements

* [PHP](https://secure.php.net/) 7.1 or newer
* [WordPress](https://wordpress.org/) 4.9 or newer