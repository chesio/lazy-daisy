/**
 * Gulp helps us grab dist files from NPM dependencies.
 */

'use strict';
const del = require('del');
const gulp = require('gulp');


/**
 * Purge assets directory.
 */
gulp.task('clean', function () {
    return del('assets');
});


/**
 * Copy Yall.js dist files and Intersection Observer polyfill into assets folder.
 */
gulp.task('vendor', function () {
    return gulp
        .src([
            'node_modules/yall-js/dist/yall.js',
            'node_modules/yall-js/dist/yall.min.js',
            'node_modules/intersection-observer/intersection-observer.js'
        ])
        .pipe(gulp.dest('assets/'))
    ;
});


/**
 * Watch for changes.
 */
gulp.task('watch', function () {
    gulp.watch('node_modules/(yall-js)/**', gulp.parallel('vendor'));
});


/**
 * Perform a clean build for deployment.
 */
gulp.task('build', gulp.series('clean', 'vendor'));


/**
 * Default task: perform a clean build and watch for changes.
 */
gulp.task('default', gulp.series('build', 'watch'));
